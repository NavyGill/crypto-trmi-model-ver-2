"""

Setting file to set up the back test process

"""


"""

Imports

"""
import os
import logging
import sys
import time
from multiprocessing import Process


"""

Params

"""
# tickers = ['NEO', 'XLM', 'BCH', 'GNT', 'BCD', 'REP', 'BCN', 'BAT', 'EOS', 'ETC', 'MKR', 'MITH', 'DGB', 'QTUM', 'STRAT', 'LSK', 'XVG', 'IOTA', 'DASH', 'ZEC', 'LTC', 'TRX', 'BNB', 'XRP', 'XMR', 'USDT', 'OMG', 'BTS', 'XEM', 'DOGE', 'BTG', 'WAVES']
# tickers = ['NEO', 'ETP', 'BCH', 'SUB', 'NOAH', 'BCD', 'PAY', 'SKY', 'HT', 'BCC', 'EOS', 'LINK', 'AAC', 'ACT', 'FUN', 'ETH', 'ETN', 'FCT', 'GNT', 'DATA', 'PPT', 'SALT', 'VTC', 'AE', 'ENG', 'ELF', 'XIN', 'LTC', 'TRX', 'R', 'ADX', 'MTL', 'VERI', 'RDN', 'SAN', 'MCO', 'MED', 'NANO', 'CNX', 'DBC', 'ZEN', 'BTX', 'BTG', 'KIN', 'SC', 'NAV']


# tickers = ['USDT', 'ACT']

tickers=['ACT','ADA','AION','ARK','BAT','BCD','BCH','BNB','BNT','BTC','BTCP','BTG','BTS','BTX','CND','CNX','CVC','DASH','DATA','DGB','DGD','EDG','ELF','EMC','EOS','ETC','ETH','ETHOS','ETP','FSN','FUN','GAME','GAS','GNO','GNT','HOT','ICN','ICX','IOST','KIN','KNC','LRC','LSK','LTC','MANA','MCO','MITH','MKR','NANO','NEO','NPXS','NXT','OMG','ONT','PAY','PIVX','POLY','PPT','QASH','QTUM','R','RDD','RDN','REP','REQ','RLC','SAN','SMART','SNGLS','SNT','STORJ','STRAT','SYS','TRX','USDT','VTC','WAVES','WAX','XEM','XLM','XMR','XRP','XTZ','XVG','XZC','ZEC','ZEN','ZIL','ZRX']
# tickers = ['ADA', 'AION', 'BTC', 'BTS', 'DGB', 'DGD', 'DOGE', 'ONT', 'QTUM', 'WAVES', 'WAX', 'EMC', 'GAME', 'GAS', 'KNC', 'LRC', 'MANA', 'OMG', 'USDT', 'XTZ']
print(len(tickers))
#full list
# tickers = ['BTC','ETH','XRP','BCH','EOS','XLM','LTC','ADA','IOTA','USDT','TRX','XMR','NEO','DASH','ETC','XEM','BNB','VET',
# 'XTZ','ZEC','OMG','QTUM','OX','ZIL','LSK','BTG','BCN','ICX','DCR','BTS','AE','MKR','ONT','DGB','DOGE','XVG','STEEM',
# 'SC','REP','BTM','BCD','BAT','GNT','STRAT','KCS','RHOC','NANO','WAVES','MITH','NPXS']
"""
# validate list
# ['NEO', 'XLM', 'BCH', 'GNT', 'BCD', 'REP', 'BCN', 'BAT', 'EOS', 'ETC', 'MKR', 'MITH', 'ETH', 'DGB', 'QTUM', 'STRAT', 'LSK', 'XVG', 'IOTA', 'DASH', 'ZEC', 'LTC', 'TRX', 'BNB', 'XRP', 'XMR', 'USDT', 'OMG', 'BTS', 'XEM', 'DOGE', 'BTG', 'BTC', 'WAVES']


Execution


"""
# old_stdout = sys.stdout
# processes = ('runscript.py')
# other = ('backtest.py',)
#
#
# # def run_process(process):
# #     os.system('python3 {}'.format(process))
#
# def script1():
#     executed = 'python3 runscript.py'
#     os.system(executed)
#     print(executed)
#
#
# def script2():
#     for ticker in tickers:
#         # pool.map(run_process, other)
#         executed = 'python3 backtest.py %s' % ticker
#         print(executed)
#         os.system(executed)
# #
# #
# if __name__ == '__main__':
#     # pool = Pool(processes=2)
#     p = Process(target=script1)
#     q = Process(target=script2)
#     p.start()
#     time.sleep(20)
#     # p.join()
#     q.start()

    # q.join()

# pool.map(run_process, processes)


# executed = 'python3 runscript.py'
# print(executed)
# os.system(executed)
#
for ticker in tickers:
    # pool.map(run_process, other)
    executed = 'python3 backtest.py %s' % ticker
    print(executed)
    os.system(executed)

