"""

Drop features from dataframe


"""


"""

Imports

"""
import pandas as pd
from sklearn.preprocessing import PolynomialFeatures

"""

Functions

"""



def get_drop_columns(df=None):
    """
    Get the columns need to be droped

    :return: columns need to be droped
    """
    result = []
    for i in range(1, 11):
            result.extend(['quandl_eod_stock_prices_Adj_open_pct_%d' % i, 'quandl_eod_stock_prices_Adj_high_pct_%d' % i, 'quandl_eod_stock_prices_Adj_low_pct_%d' % i,
                           'quandl_eod_stock_prices_Adj_close_pct_%d' % i])
    return result


def get_y_colums(horizon, multi_label=False):
    """
    Get target columns

    :param horizon: prediction horizon
    :param multi_label: whether we predict multiple labels
    :return: column names for target
    """
    result = []
    index = horizon + 1
    if multi_label:
        result.extend(['quandl_eod_stock_prices_Adj_open_pct_%d' % index, 'quandl_eod_stock_prices_Adj_high_pct_%d' % index, 'quandl_eod_stock_prices_Adj_low_pct_%d' % index, 'quandl_eod_stock_prices_Adj_close_pct_%d' % index])
    else:
        result.append('quandl_eod_stock_prices_Adj_close_pct_%d' % index)
    return result



