"""

Back test script

"""


"""

Imports

"""
import itertools

import os
os.environ['KERAS_BACKEND'] = 'theano'
import pandas as pd
from data_processing import load_train_test_data, data_selector, generate_pct_from_df, add_technical_indicator, pca, get_important_features, get_stock_data
from keras import backend as K
from NN_Models import create_model_lstm, create_model_cnn1, create_model_cnn2
from keras.models import load_model

import datetime
from holidays import UnitedStates
from dateutil.relativedelta import relativedelta
import numpy as np
import sys
from backtest_util import BacktestUtil
from predict_backtest_common_function import *
from RuntimeSettting import SAVE_RESULTS, SAVE_LOCAL
from sklearn import metrics




"""

Params

"""
USE_MULTI_LSTM = 1
backtest_length = 1

ticker = sys.argv[1]
horizons = [0,1,2,3]


seq_len = 30

MODEL = 'CNN1'
shape = [4, seq_len, 1]
params = [[32, 16, 8, 1, 1], [32, 16, 8, 1, 1]]
dropout = 0.3
batch_size = 100
epochs = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1] # todo: change the params
# epochs = [8, 8, 8, 8, 8, 8, 8, 8, 8, 8]

MODEL_VERSION = '0.1'

def predict(model, test_X):
    """
    Get prediction result

    :param model: the model we created
    :param test_X: data points used for prediction
    :return: prediction results
    """
    p = model.predict(test_X)
    print('hhhhhhhhhhhhhhhhhhhhhhhhssssssssssssssssss', test_X)
    print(p)
    return p

def generate_df_with_pct_and_TA(df, df_indicator):

    df_pct = generate_pct_from_df(df,10)
    df_pct.append(df_indicator)
    return pd.concat(df_pct, axis=1, join_axes=[df.index])



def get_rmse( y, y_fit ):
  return np.sqrt( metrics.mean_squared_error( y, y_fit ) )



def main():
    backtest_model_path = get_backtest_path_safely(MODEL)
    backtester = BacktestUtil(ticker,max(horizons)+1)
    df = get_data(ticker)
    df = df.loc[datetime.date(year=2017, month=9, day=1):datetime.date(year=2018, month=9, day=20)]
    df = df[~df.index.duplicated(keep='first')]
    # if ticker in tickers1:
    #     print(df.columns)
    #     df = df[['quandl_eod_stock_prices_Open', 'quandl_eod_stock_prices_High', 'quandl_eod_stock_prices_Low', 'quandl_eod_stock_prices_Close', 'quandl_eod_stock_prices_Volume', 'quandl_eod_stock_prices_Dividend', 'quandl_eod_stock_prices_Split', 'quandl_eod_stock_prices_Adj_Volume', 'quandl_eod_stock_prices_Adj_Open', 'quandl_eod_stock_prices_Adj_High', 'quandl_eod_stock_prices_Adj_Low', 'quandl_eod_stock_prices_Adj_Close']]
    # else:
    #     pass

    # print(df)
    print("ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt")
    df = add_technical_indicator(df)
    # drop duplicate index
    df = drop_duplicated_index(df)
    # ft=[]
    # ft.append(df.columns)
    print("columns previous", list(df.columns.values))
    # df, components = pca(df)
    # features_drop = get_important_features(df1, components, df.columns.values)
    # print(features_drop)
    # df.drop(features_drop, axis=1, inplace=True)
    # df = keep_price_columns(df)
    # print("columns now", list(df.columns.values))
    # split the features into two lstm network

    timestamps = df.index.tolist()
    dates = timestamps[-backtest_length-1:]
    df_indicator = df[[col for col in df if col.startswith('ta_')]]
    df_original = df.copy()
    # simple process, normalization, pct
    df = generate_df_with_pct_and_TA(df, df_indicator)
    print("testing df", len(df.columns))
    print("df_temp", list(df.columns.values))
    rmse_df = pd.DataFrame(columns=["degree", "rmse_train", "rmse_test"])

    for i in range(1, len(dates)):
        # get date info
        date = dates[i]
        prev_date = dates[i-1]
        print("preeeeeeeeeeeeeeeeeeeeeee", prev_date)

        # prepare backtest dataframe
        temp_location = df.index.get_loc(date)
        print(temp_location)
        df_temp = df.ix[:temp_location+1]
        print("df_temp", len(df_temp.columns))
        df_original_temp = df_original.ix[:temp_location+1]
        print("df_original", len(df_original_temp.columns))

        # run prediction for different horizon
        for horizon in horizons:
            print("This is %s's %s-days ahead prediction, using %s model." % (ticker, (horizon+1), MODEL))
            if i == 1 and os.path.isfile(backtest_model_path + '%s_%s_%sday_ahead_Model.h5' % (ticker, MODEL, horizon + 1)) == False:
                train_X, train_y, test_X, last_date, last_prices = load_train_test_data(df_temp, df_original_temp, None, None,
                                                                                        horizon, seq_len)
            else:
                df_pred, df_train = data_selector(df_temp, prev_date, horizon, seq_len)
                train_X, train_y, test_X, last_date, last_prices = load_train_test_data(df_temp, df_original_temp, df_pred, df_train,
                                                                                        horizon, seq_len)
            RE_TRAIN = 0
            model_file_name = backtest_model_path + '%s_%s_%sday_ahead_Model.h5' % (ticker, MODEL, horizon + 1)
            if os.path.isfile(model_file_name):
                model = load_model(model_file_name)
                print('load %s model successfully' % MODEL)
                model.compile(loss='mse', optimizer='adam', metrics=['mae'])
                print("model-weights",model.get_weights()[0])
                # get_output = K.function([model.layers[0].input],[model.layers[7].output])
                # print(get_output([0])[0])
                RE_TRAIN = 1
            else:
                if MODEL == 'CNN1':
                    model = create_model_cnn1(shape, params[0], dropout)
                if MODEL == 'CNN2':
                    model = create_model_cnn2(shape, params[0], dropout)
                if MODEL == 'LSTM':
                    model = create_model_lstm(shape, params[0], dropout)

            # model.summary()
            # train_X = train_X.reshape(1,30,4)
            print('train size: ', train_X.shape)
            print('test size: ', test_X.shape)
            print('labels size:', train_y.shape)

            last_date = str(last_date)[:10]
            last_date = last_date.split('-')
            if RE_TRAIN:
                model.fit(train_X, train_y, batch_size=1, epochs=8, verbose=1)
                print("model-weights-after-training", model.get_weights()[1])
            else:
                model.fit(train_X, train_y, batch_size=batch_size, epochs=epochs[horizon], verbose=1)
                print("model-weights-after-training", model.get_weights()[1])
            model.save(backtest_model_path + '%s_%s_%sday_ahead_Model.h5' % (ticker, MODEL, horizon + 1))

            # get prediction result numbets
            pred_num = test_X.shape[0]

            for num in range(0, pred_num):
                df_list=[]
                print('********************************************************************')
                print('The data used for prediction: ', test_X[num])
                test_X = np.array([test_X[num].tolist()])
                pct = predict(model, test_X)
                last_price = last_prices.iloc[num]

                print('previous actual price is: ', last_price)
                print('********************************************************************\n\n')
                pred_date=[]
                pred_results = []



                # do the de-nor malization
                p = last_price + pct * last_price

                # get date time
                last_datetime = datetime.datetime(int(last_date[0]),int(last_date[1]),int(last_date[2]))+datetime.timedelta(hours=21, minutes=0, seconds=0)
                computation_date = datetime.datetime.utcnow()
                prediction_date = last_datetime + relativedelta(days=horizon+1)
                horizon_value = (horizon + 1) * 86400
                prediction_result = float(p[0][0])
                if str(prediction_date)[:10] in df_original.index:
                    real_price = df_original.ix[str(prediction_date)[:10]]['quandl_eod_stock_prices_Close']
                else:
                    real_price = np.nan
                #if SAVE_LOCAL:
                #    backtester.push_predicted_data(horizon, prediction_result, real_price, prediction_date)

                print('##########################################################################################################')
                print('The last date that we have actual price and info is: ', last_date)
                print('It is %s days ahead prediction.' % (horizon+1))
                print('prediction date is: ', prediction_date)
                print('prediction result is: ', prediction_result)
                print('##########################################################################################################')
                print(type(prediction_result))
                pred_date.append(prediction_date)
                floatprediction = np.array(pred_results, dtype=np.float32)

                # merged = list(itertools.chain.from_iterable(prediction_result))
                floatprediction= np.append(floatprediction, prediction_result)
                print(prev_date, floatprediction)

                percentile_list = pd.DataFrame({'timestamp_utc': prev_date, 'prediction_results': floatprediction})
                df_list.append(percentile_list)
                percentile_list.to_csv(ticker+"_5.csv", index=False)
                if SAVE_RESULTS:
                    save_result(MODEL+'_crypto_trmi_vader', MODEL_VERSION, ticker, prediction_result, prediction_date, horizon_value)
        if SAVE_LOCAL:
            backtester.push_start_date(date)
            backtester.update_result()
    print(df_list)

if __name__ == '__main__':
    main()
