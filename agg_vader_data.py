import pandas as pd



data_path = 'MomentumWeb/data/'

# df1 = pd.read_csv(data_path+'2016_9.csv', index_col='date')
# df2 = pd.read_csv(data_path+'2016_10.csv', index_col='date')
# df3 = pd.read_csv(data_path+'2016_11.csv', index_col='date')
# df4 = pd.read_csv(data_path+'2016_12.csv', index_col='date')
# df5 = pd.read_csv(data_path+'2017_1.csv', index_col='date')
# df6 = pd.read_csv(data_path+'2017_2.csv', index_col='date')
# df7 = pd.read_csv(data_path+'2017_3.csv', index_col='date')
# df8 = pd.read_csv(data_path+'2017_4.csv', index_col='date')
# df9 = pd.read_csv(data_path+'2017_5.csv', index_col='date')
# df10 = pd.read_csv(data_path+'2017_6.csv', index_col='date')
# df11 = pd.read_csv(data_path+'2017_7.csv', index_col='date')
# df12 = pd.read_csv(data_path+'2017_8.csv', index_col='date')
# df13 = pd.read_csv(data_path+'2017_9.csv', index_col='date')
# df14 = pd.read_csv(data_path+'2017_10.csv', index_col='date')
# df15 = pd.read_csv(data_path+'2017_11.csv', index_col='date')
# df16 = pd.read_csv(data_path+'2017_12.csv', index_col='date')
# df17 = pd.read_csv(data_path+'2018_1.csv', index_col='date')
# df18 = pd.read_csv(data_path+'2018_2.csv', index_col='date')
# df19 = pd.read_csv(data_path+'2018_3.csv', index_col='date')
# df20 = pd.read_csv(data_path+'2018_4.csv', index_col='date')
# df21 = pd.read_csv(data_path+'2018_5.csv', index_col='date')
# df22 = pd.read_csv(data_path+'2018_6.csv', index_col='date')
# df23 = pd.read_csv(data_path+'2018_7.csv', index_col='date')
# df24 = pd.read_csv(data_path+'2018_8.csv', index_col='date')
# df25 = pd.read_csv(data_path+'2018_9.csv', index_col='date')

# df = pd.concat([df1, df2, df3, df4, df5, df6, df7, df8, df9, df10,
#                 df11, df12, df13, df14, df15, df16, df17, df18,df19,
#                 df20, df21, df22, df23, df24, df25])

df_new = pd.read_csv(data_path+'data.csv', index_col='date')
df_concat = pd.read_csv(data_path+'concat_vader.csv', index_col='date')
df = pd.concat([df_concat, df_new])
df.to_csv(data_path+'data.csv')