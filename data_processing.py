from aicryptoSDK import AICrypto
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from talib import MACD,BBANDS,ATR,RSI,STOCHRSI,ADX,DX
import math
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler, normalize,MaxAbsScaler

import pandas as pd
# from predict_backtest_common_function import read_datasource
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
aicrypto = AICrypto(access_token="aWnkBW0teY")
days_ahead = 5


def add_pct_change(df):
    df_pcts = generate_pct_from_df(df,days_ahead)
    return pd.concat(df_pcts, axis=1, join_axes=[df.index])


def add_technical_indicator(df):
    atr = ATR(df['quandl_eod_stock_prices_High'].as_matrix(), df['quandl_eod_stock_prices_Low'].as_matrix(), df['quandl_eod_stock_prices_Close'].as_matrix())
    rsi = RSI(df['quandl_eod_stock_prices_Close'].as_matrix())
    # stochastic = STOCHRSI(df['Close'].as_matrix())
    adx = ADX(df['quandl_eod_stock_prices_High'].as_matrix(), df['quandl_eod_stock_prices_Low'].as_matrix(), df['quandl_eod_stock_prices_Close'].as_matrix())
    direction_index = DX(df['quandl_eod_stock_prices_High'].as_matrix(), df['quandl_eod_stock_prices_Low'].as_matrix(), df['quandl_eod_stock_prices_Close'].as_matrix())
    df['ta_rsi'] = rsi
    df['ta_atr'] = atr
    # df['ta_stochastic'] = stochastic
    df['ta_adx'] = adx
    df['ta_dx'] = direction_index
    df_vader = pd.read_csv('MomentumWeb/data/data.csv')
    df_vader = df_vader.rename(columns={'compound_sent':'ta_vader'})
    df = pd.concat([df, df_vader], axis=1, join_axes=[df.index])
    df = df.fillna(0)
    print (df.head(10))
    return df

def normalize1(X):
    max=0.9
    min=0.1
    X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
    X_scaled = X_std * (max - min) + min
    return X_scaled

def pca(df):
    df = df.fillna(0)
    df = df.replace(np.inf, 0)
    df = df.replace(-np.inf, 0)
    # print(df.describe())
    X = StandardScaler().fit_transform(df)  # standardize the data
    scale = preprocessing.minmax_scale(df, feature_range=(0.1, 0.9))
    # nor = normalize1(df)
    # print("kllllllllsddddddddddddd", nor)

    # rescaledX = scale.fit_transform(df)
    # # print("jjjjjjjjjjjjjjjjjjjjjjjjjjjj", rescaledX[0])
    #
    # normalized_X = normalize(rescaledX, norm='l1', axis=1, copy=True)

    mean_vec = np.mean(X, axis=0)
    cov_mat = (X - mean_vec).T.dot((X - mean_vec)) / (X.shape[0] - 1)
    # print('Covariance matrix \n%s' %cov_mat)
    cov_mat = np.cov(X.T)
    eig_vals, eig_vecs = np.linalg.eig(cov_mat)
    # print('Eigenvectors \n%s' % eig_vecs)
    # print('\nEigenvalues \n%s' % eig_vals)

    eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:, i]) for i in range(len(eig_vals))]
    # print('Eigenvalues in descending order:')
    # for i in eig_pairs:
    #     print(i[0])
    pca = PCA(n_components=4)
    t = pca.fit_transform(scale)
    # print("hello", t)
    print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",t)
    return t, pca.components_

def get_important_features(transformed_features, components_, columns):
    """
    This function will return the most "important"
    features so we can determine which have the most
    effect on multi-dimensional scaling
    """
    num_columns = len(columns)
    print(num_columns)
    drop_ft = []

    # Scale the principal components by the max value in
    # the transformed set belonging to that component
    xvector = components_[0] * max(transformed_features[:,0])
    yvector = components_[1] * max(transformed_features[:,1])

    # Sort each column by it's length. These are your *original*
    # columns, not the principal components.
    important_features = { columns[i] : math.sqrt(xvector[i]**2 + yvector[i]**2) for i in range(num_columns) }
    important_features = sorted(zip(important_features.values(), important_features.keys()), reverse=True)
    print ("Features by importance:\n", important_features)
    print("Length of Features:\n", len(important_features))
    for i in range(len(important_features)):
        score = important_features[i][0]
        if score <=0:
            print(i)
            print(important_features[i])
            drop_features = important_features[i][1]
            print(drop_features)
            drop_ft.append(drop_features)
            if "quandl_eod_stock_prices_High" in drop_ft:
                drop_ft.remove("quandl_eod_stock_prices_High")
            if "quandl_eod_stock_prices_Open" in drop_ft:
                drop_ft.remove("quandl_eod_stock_prices_Open")
            if "quandl_eod_stock_prices_Low" in drop_ft:
                drop_ft.remove("quandl_eod_stock_prices_Low")
            if "quandl_eod_stock_prices_Close" in drop_ft:
                drop_ft.remove("quandl_eod_stock_prices_Close")
    return drop_ft

#
# def get_df_with_ta():
#     df = read_from_csv()
#     df = add_technical_indicator(df)
#     df = add_pct_change(df)
#     return df


def get_pct_feature_for_day(day):
    return {'quandl_eod_stock_prices_Adj_Open': 'quandl_eod_stock_prices_Adj_open_pct_%d' % day,
            'quandl_eod_stock_prices_Adj_High': 'quandl_eod_stock_prices_Adj_high_pct_%d' % day,
            'quandl_eod_stock_prices_Adj_Low': 'quandl_eod_stock_prices_Adj_low_pct_%d' % day,
            'quandl_eod_stock_prices_Adj_Close': 'quandl_eod_stock_prices_Adj_close_pct_%d' % day}


def generate_pct_from_df(df, days):
    # df.reset_index(level=0, inplace=True)
    # df1 = df.drop(['timestamp_utc'], axis=1)
    print("collllllllllllllllllllll", df.columns)

    result = [None for i in range(days)]
    scale = [None for i in range(days)]
    final_list = []
    index_list =[]
    final_list_col = [None for i in range(days)]
    rescaledX = [None for i in range(days)]
    col_list = [None for i in range(days)]
    normalized_X = [None for i in range(days)]
    gh = [None for i in range(days)]


    # col = df.columns
    # col_list = pd.Index.tolist(col)

    for i in range(days):
        print("noooooooooooooooooooooooooooooo", i)
        result[i] = df.pct_change(i+1).fillna(0.0).rename(columns=get_pct_feature_for_day(i+1))
        # gh[i] = df.index
        # # print(pd.DataFrame(result[0]).head())
        # t = pd.DataFrame(result[i])
        # # print("cooooooooooooooooooooooooooooo", t.columns)
        # col = t.columns
        # print(col)
        # col_list = pd.Index.tolist(col)
        #
        # final_list_col.append(col_list)
        #
        #
        # result[i] = np.nan_to_num(result[i])
        # # print("gggggddddddddddddddddddddddddddddjjjjjjjjjjjjjjjjj",result[0])
        # print(result[i].shape)
        # scale[i] = MinMaxScaler(feature_range=(0, 1))
        # rescaledX[i] = scale[i].fit_transform(result[i])
        # # print("jjjjjjjjjjjjjjjjjjjjjjjjjjjj", rescaledX[0])
        #
        # normalized_X[i] = normalize(rescaledX[i], norm='l1', axis=1, copy=True)
        # # print("vvvvvvvvvvvvvvvvvvvvvvvvv", normalized_X[0])
        # # print(scale[i].shape)
        #
        #
        # df_1 = pd.DataFrame(normalized_X[i], columns=col_list)
        #
        # # df_1 = df_1.index.drop()
        # df_2 = pd.DataFrame(gh[i], columns=["timestamp_utc"])
        # sd = df_2['timestamp_utc'].values
        # print(type(sd))
        # # df_2 = df_2.index.drop()
        # df_1['timestamp_utc'] = sd
        #
        # df_1= df_1.set_index('timestamp_utc')
        # # print("kkkkkkkkkkkkkkkkkkkkkkkkkk",df_1.head())
        #
        #
        # final_list.append(df_1)
        # print("gbbbbbbbbbbbbbbbbbbbbbbbbbbbb",type(result))
        # print("geeeeeeeeeeeeeeeeeeeeeeeeeeeeee", final_list[0].head())
    return result



"""

Processing the Dataframe

"""


"""

Imports

"""
from aicryptoSDK import AICrypto

import pandas as pd
from keras.callbacks import *
from feature_utils import get_y_colums, get_drop_columns


"""

Functions

"""
def data_selector(df, prev_date, horizon, seq_len):
    """
    Divide the original dataframe into training set and prediction set
    :param df: original dataframe
    :param prev_date: the latest date used for prediction
    :param horizon: prediction horizon
    :param seq_len: window length for input
    :return: training set and prediction set or original dataframe if missing latest date
    """
    # prepare new prediction set

    if prev_date:
        location = df.index.get_loc(prev_date)
        print("datttttttttttttttttttttt", location)
        temp1 = df.ix[:location+2]
        temp2 = df.ix[df.index.difference(temp1.index)]
        temp1 = temp1.ix[-seq_len:]
        df_pred = pd.concat([temp1, temp2])
        # prepare new training set
        location_new_start = location + 1
        print(type(horizon))

        # location_new_end = df.index.get_loc(df.index.values[-1])

        df_new_train = df.ix[location_new_start-seq_len-horizon:]
        print("fgfgfffffffffffffffdsddddddddddddddddddddd", (location_new_start-seq_len-horizon))
        print(len(df_new_train.columns), len(df_pred.columns))
        return df_pred, df_new_train

    else:
        print('No previous date info, return original dataframe')
        return df




def get_stock_data(stock_name, horizon, seq_len, normalize=True, sma=[],df_sdk = None):
    """
    Get data for specific ticker and add all features into it

    :param stock_name: ticker name
    :param horizon: prediction horizon
    :param seq_len: input layer window length
    :param normalize: whether we add percentage features into it
    :param sma:
    :param df_sdk: if df_sdk is not None, the df don't need to download from sdk
    :return: processed dataframe, original dataframe, train set, prediction set
    """
    df = None
    if df_sdk is not None:
        print('I have data from SDK already, I won\'t download data')
        df = df_sdk.copy()
    else:
        aicrypto = AICrypto(access_token="aWnkBW0teY")
        df = aicrypto.read.data(
            datasource="quandl_bnc2_stock_prices+marketpsych_crypto",
            version="0.1",
            ticker=stock_name,
            resolution="1D"
        )
    df = df[~df.index.duplicated(keep='first')]
    # df = add_technical_indicator(df)
    # print("ggggggggggggggggggggggggggggg")
    # df1, components = pca(df)
    # features_drop = get_important_features(df1, components, df.columns.values)
    # # print(features_drop)
    # df.drop(features_drop, axis=1, inplace=True)
    df.to_csv('data/%s_data_file.csv' % stock_name)

    previous_df = pd.read_csv('data/%s_data_file.csv' % stock_name)
    previous_df = previous_df[~previous_df.index.duplicated(keep='first')]
    previous_index = previous_df['timestamp_utc'].iloc[-2]

    # drop duplicate index

    # df = df[~df.index.duplicated(keep='first')]

    # df.to_csv('data/%s_data_file.csv' % stock_name)
    # df_market = df[[col for col in df if col.startswith('marketpsych_')]]
    df_indicator = df[[col for col in df if col.startswith('ta_')]]
    df = df[['quandl_eod_stock_prices_Close','quandl_eod_stock_prices_High','quandl_eod_stock_prices_Low','quandl_eod_stock_prices_Open']]

    df_original = df.copy()

    # simple process, normalization, pct
    if normalize:
        df_pct = generate_pct_from_df(df, 10)
        df_pct.append(df_indicator)
        df = pd.concat(df_pct, axis=1, join_axes=[df.index])

        # if previous_index:
        df_pred, df_new_train = data_selector(df, previous_index, horizon, seq_len)

    return df, df_original, df_pred, df_new_train


def load_data(stock, seq_len, horizon):
    """
    Load data into matrix

    :param stock: input dataframe
    :param seq_len: input window length
    :param horizon: prediction horizon
    :return: train sets:X, Y; test sets:X, Y; split index
    """
    data = stock.as_matrix()
    sequence_length = horizon + seq_len + 1 # index starting from 0
    result = []

    for index in range(1, (len(data) - sequence_length+1)): # maxmimum date = lastest date - sequence length
        result.append(data[index: index + sequence_length]) # index : index + 20days

    result = np.array(result)
    row = round(0.9 * result.shape[0]) # 90% split
    train = result[:int(row), :] # 90% date
    test = result[int(row):, :]
    print(train.shape)
    X_train = train[:, :(sequence_length-horizon-1), :]


    col_num = (horizon + 1) * 5 - 1
    y_train = train[:, -1][:, col_num] # day m + 1 adjusted close price

    X_test = result[int(row):, :(sequence_length-horizon-1),:]
    y_test = result[int(row):, -1][:, col_num]

    print(X_train.shape)
    print(y_train.shape)
    print(X_test.shape)
    print(y_test.shape)

    return [X_train, y_train, X_test, y_test, int(row)]


def load_train_test_data(df_source, df_original, df_pred, df_new_train, horizon, seq_len, use_multi_label=False):
    """
    Divide the data into train sets and prediction sets

    :param df_source: dataframe contains all the features
    :param df_original: original dataframe
    :param df_pred: dataframe used for prediction
    :param df_new_train: dataframe used for re-train
    :param horizon: prediction horizon
    :param seq_len: input window length
    :param use_multi_label: whether to use multiple labels
    :return: train sets:X, Y; prediction sets; last date point; target data point for denormalization
    """
    train_X = None
    train_y=None
    test_X=None
    last_date=None
    last_y = None
    if df_pred is None or len(df_pred) == 0 or len(df_new_train) == 0:
        train_X, train_y, test_X, last_date, last_y = load_old_data(df_source, df_original, seq_len, horizon, use_multi_label)
    else:
        train_X, train_y, test_X, last_date, last_y = load_new_coming_data(df_source, df_original, df_pred, df_new_train, seq_len, horizon, use_multi_label)
    return train_X, train_y, test_X, last_date, last_y


def load_old_data(df_source, df_original, seq_len, horizon, use_multi_label=False):
    """
    When there is no new coming data or train first time, divide by the whole dataframe

    :param df_source:
    :param df_original:
    :param seq_len:
    :param horizon:
    :param use_multi_label:
    :return: train sets: X, Y; predict sets; last date; percentage change target data points
    """
    train_X = []
    train_y = []
    test_X = []
    last_price = None

    drop_features = get_drop_columns(df_source)

    y_columns = get_y_colums(horizon,use_multi_label)
    source_len = len(df_source)
    print("ddddddddddddddddddddddddd", df_source)
    df_X = df_source.drop(drop_features, axis=1)
    print("aaaaaaaaaaaaaaaaaaaaaaaaaaa", df_X)

    df_X, components = pca(df_X)
    df_y = df_source[y_columns]
    test_X.append(df_X[-seq_len:])
    test_X = np.array(test_X)
    for i in range(0,source_len-horizon-seq_len):
        train_X.append(df_X[i:i+seq_len])
        train_y.append(df_y.iloc[i+seq_len].as_matrix())
    train_X = np.array(train_X)
    train_y = np.array(train_y)
    print("training data")
    print(train_X)
    print(train_y.shape)
    print(train_X.shape)
    print(test_X.shape)

    if use_multi_label:
        last_price = df_original.iloc[-1:]
    else:
        last_price = df_original.iloc[-1:]['quandl_eod_stock_prices_Close']
    return train_X, train_y, test_X, df_source.index.values[-1], last_price


def load_new_coming_data(df_source, df_original, df_pred, df_new_train, seq_len, horizon, use_multi_label=False):
    """
    When there is new coming data, divide the new data

    :param df_source: dataframe contains all features
    :param df_original: original dataframe
    :param df_pred: data points used for prediction
    :param df_new_train: coming data used for training
    :param seq_len: input window length
    :param horizon: prediction horizon
    :param use_multi_label: whether to use multiple labels
    :return: train sets: X, Y; predict sets; last date; percentage change target data points
    """
    train_X= []
    train_y = []
    test_X = []
    last_date = None
    last_price = None
    test_len = len(df_pred) - seq_len + 1
    pred_index = df_pred.index.values
    last_date = pred_index[seq_len-1]
    drop_features = get_drop_columns(df_source)
    df_pred = df_pred.drop(drop_features, axis=1)


    df_pred, components = pca(df_pred)
    for i in range(0, test_len):
        previous_days = df_pred[i: i + seq_len]
        # previous_days = previous_days.drop(drop_features, axis=1)
        test_X.append(previous_days)
    test_X = np.array(test_X)
    train_len = len(df_new_train) - seq_len - horizon
    y_columns = get_y_colums(horizon, use_multi_label)

    df_train_copy = df_new_train.copy()
    # print("traaaaaaaaaaaaaaaaaaaa", df_train_copy.head())

    try:
        df_train_copy = df_train_copy.drop(drop_features, axis=1)
    except:
        df_train_copy = df_train_copy
    # print(df_train_copy.head())
    # df_train_copy = df_train_copy.replace(np.inf, 0)
    # print(df_train_copy.describe())
    df_train_copy, components = pca(df_train_copy)
    print("new_train", df_new_train[y_columns].head())

    # print("training_copy", list(df_train_copy.columns.values))
    # except:
    #     pass
    for i in range(0, train_len):
        train_X.append(df_train_copy[i:i+seq_len])
        train_y.append(df_new_train[y_columns].iloc[i+seq_len+horizon:i+seq_len+1+horizon].as_matrix()[0])
    # print("hgggggggggggggggggggggggg", train_X)
    # print("gbbbbbbbbbbbbbbbbbbbbbbbbb", train_y)
    # train_frame = pd.DataFrame(train_X, train_X)
    # print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhh", train_frame)
    train_X = np.array(train_X)
    a,b,c = train_X.shape
    if c>4:
        print("hgdhdghgzgghj")
    print("training data", train_X)
    train_y = np.array(train_y)
    print(train_y.shape)
    print(train_X.shape)

    print(test_X.shape)

    new_index = pred_index[seq_len-1:]
    if use_multi_label:
        last_price = df_original.ix[new_index]
    else:
        last_price = df_original.ix[new_index]['quandl_eod_stock_prices_Close']
    print(last_price.index.values)
    return train_X, train_y, test_X, last_date, last_price


def convert_pct_to_price(pcts, last_price):
    """
    Convert percentage changes into close price

    :param pcts: percentage changes
    :param last_price: percentage change target price
    :return: de-normalized prediction result
    """
    result = []
    for p in pcts:
        result.append(last_price + last_price*p)
    return result

