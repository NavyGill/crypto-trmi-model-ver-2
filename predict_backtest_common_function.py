from aicryptoSDK import AICrypto
import pandas as pd
import os
import quandl


aicrypto = AICrypto(access_token="aWnkBW0teY")


# aicrypto = AICrypto(access_token="aWnkBW0teY")
def get_data(ticker_name):
    df = aicrypto.read.data(
        datasource="quandl_bnc2_stock_prices+marketpsych_crypto",
        version="0.1",
        ticker=ticker_name,
        resolution="1D"
        )
    return df

import glob
# watstock = Watstock(access_token="aWnkBW0teY")




def read_datasource(ticker,start_date = None, end_date = None):
    quandl.ApiConfig.api_key = 'wbscomxyg4HCnPH6vwnZ'
    df = quandl.get('BNC2/MWA_%s_USD'%ticker, start_date=start_date, end_date=end_date)
    df_tele = read_sentiment()
    merged = pd.merge(df_tele, df, how='left', left_index=True,right_index=True)
    return merged



def read_sentiment():
    path = 'telegram_sentiments'  # use your path
    allFiles = glob.glob(path + "/*.csv")
    frame = pd.DataFrame()
    list_ = []
    for file_ in allFiles:
        df = pd.read_csv(file_, index_col=None, header=0)
        list_.append(df)

    frame = pd.concat(list_)
    frame['date'] = pd.to_datetime(frame['date'])
    frame = frame.rename(columns={'date': 'datetime','compound_sent':'sentiments'})
    frame = frame.sort_values(by=['datetime'])
    frame = frame.append(read_from_server())
    frame = frame.rename(columns={'datetime': 'Date','sentiment':'ta_sentiment'})
    frame = frame.set_index('Date')
    return frame



def read_from_server():
    import requests
    import datetime
    import json
    today = datetime.datetime.today()
    m = today.month
    y = today.year
    d = today.day
    start = datetime.datetime.strptime("2018-09-01", "%Y-%m-%d")
    date_generated = [(start + datetime.timedelta(days=x)) for x in range(0, (today - start).days + 1)]
    r = requests.post('http://localhost:5000/predict', data={'Y_start': 2018,'M_start':9,'D_start':1,'Y_end':y,'M_end':m,'D_end':d})
    content = r.text
    js = json.loads(content)
    sentiments = js['sentiment']
    new_df = pd.DataFrame.from_dict({'datetime':date_generated,'sentiments':sentiments})
    return new_df


# def read_datasource(ticker,start_date = None, end_date = None):
#     quandl.ApiConfig.api_key = 'wbscomxyg4HCnPH6vwnZ'
#     df = quandl.get('BNC2/MWA_%s_USD'%ticker, start_date=start_date, end_date=end_date)
#     return df



def save_result(model_name, model_version, ticker_name, prediction_result, prediction_date, horizon_value):
    aicrypto.write.prediction(model=model_name,
                              version=model_version,
                              ticker=ticker_name,
                              prediction=prediction_result,
                              prediction_date=prediction_date,
                              horizon=horizon_value
                              )


def drop_duplicated_index(df):
    return df[~df.index.duplicated(keep='first')]


def keep_price_columns(df):
    return df[
        ['quandl_eod_stock_prices_High',
         'quandl_eod_stock_prices_Low',
         'quandl_eod_stock_prices_Close',
         'quandl_eod_stock_prices_Open']
    ]

from RuntimeSettting import IN_SERVER
def get_backtest_path_safely(model_name):
    if IN_SERVER:
        backtest_model_path = '/home/watstock/modules_combined/%s/backtest_model/crypto_model_saved/' % model_name
    else:
        backtest_model_path = 'data/%s/' % model_name
    # Make sure backtest folder is exist
    if not os.path.exists(backtest_model_path):
        os.makedirs(backtest_model_path)
    return backtest_model_path


