import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_absolute_error, mean_squared_error
import os


class BacktestUtil:

    def __init__(self, ticker_name, days_ahead):
        self.ticker_name = ticker_name
        self.predicted_prices = [[] for i in range(days_ahead)]
        self.real_prices = [[] for i in range(days_ahead)]
        self.start_dates = []
        self.predicted_dates = [[] for i in range(days_ahead)]

    @staticmethod
    def _get_root_dir_name(ticker):
        path = 'backtest_result/%s/' % ticker
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    @staticmethod
    def _create_csv_columns_name(num):
        result = []
        for i in range(num):
            result.append('%ddays_ahead' % (i+1))
        return result

    @staticmethod
    def _varied_length_list_to_array(the_list,filled_value=np.nan):
        max_len = len(sorted(the_list, key=len, reverse=True)[0])
        if len(the_list) == 1:
            list_filled = np.array(the_list)
        else:
            list_filled = np.array([xi + [filled_value] * (max_len - len(xi)) for xi in the_list])
        return list_filled

    @staticmethod
    def _check_list_type(the_list):
        if len(the_list) == 0:
            return None
        list_len = len(the_list)
        for i in range(list_len):
            if len(the_list[i]) > 0:
                return type(the_list[i][0])
        return float

    def write_price_pic(self, ticker, predicted_prices, actual_prices, dates=None):
        assert len(actual_prices) == len(predicted_prices)
        predicted_prices = self._varied_length_list_to_array(predicted_prices)
        actual_prices = self._varied_length_list_to_array(actual_prices)
        if dates is not None:
            assert len(predicted_prices[0] == len(dates))
        for i in range(0, len(actual_prices)):
            plt.plot(predicted_prices[i])
            plt.plot(actual_prices[i])
            if dates is not None:
                plt.xticks(range(len(dates)), dates, rotation='vertical')
            column_names = ['predicted', 'actual_price']
            plt.legend(column_names)
            plt.savefig(self._get_root_dir_name(ticker) + '%ddays_ahead.png'%(i+1))
            plt.close()

    def write_csv(self, ticker, predicted, real_prices, start_dates=None, result_dates=None):
        assert (len(predicted) == len(real_prices))
        predicted = self._varied_length_list_to_array(predicted)
        real_prices = self._varied_length_list_to_array(real_prices)
        result_dates = self._varied_length_list_to_array(result_dates)
        dates_ahead = len(predicted)
        columns_predicted = ['%ddays_ahead_predicted' % (i + 1) for i in range(dates_ahead)]
        columns_actual = ['%ddays_ahead_actual' % (i + 1) for i in range(dates_ahead)]

        if result_dates is not None:
            columns_date = ['%ddays_ahead_date' % (i + 1) for i in range(dates_ahead)]
            columns = np.array(zip(columns_date, columns_predicted, columns_actual,)).reshape(-1)
            result = np.array(zip(result_dates, predicted, real_prices)).reshape(dates_ahead * 3, predicted.shape[1]).T
        else:
            result = np.array(zip(predicted, real_prices)).reshape(dates_ahead*2, predicted.shape[1]).T
            columns = np.array(zip(columns_predicted,columns_actual)).reshape(-1)
        if start_dates is None:
            df = pd.DataFrame(result, columns=columns)
        else:
            df = pd.DataFrame(result, columns=columns, index=start_dates)
        file_name = self._get_root_dir_name(ticker) + '%s_result.csv' % ticker
        df = df.dropna(axis=1, how='all')
        df.to_csv(file_name)

    def calculate_error(self, ticker, predicted_results, actual_prices, dates=None):
        assert len(actual_prices) == len(predicted_results)
        predicted_results = self._varied_length_list_to_array(predicted_results, filled_value=0)
        actual_prices = self._varied_length_list_to_array(actual_prices, filled_value=0)
        result = []
        for i in range(0, len(actual_prices)):
            combined = np.concatenate(([predicted_results[i]], [actual_prices[i]]))
            combined = combined[:, ~np.isnan(combined).any(axis=0)]
            if len(combined[0]) > 0: # if without this, there will be expection when days < 5
                result.append(mean_squared_error(combined[0], combined[1]))
            else:
                result.append(np.nan)
        for i in range(0, len(predicted_results)):
            combined = np.concatenate(([predicted_results[i]], [actual_prices[i]]))
            combined = combined[:, ~np.isnan(combined).any(axis=0)]
            if len(combined[0]) > 0:  # if without this, there will be expection when days < 5
                result.append(mean_absolute_error(combined[0], combined[1]))
            else:
                result.append(np.nan)
        columns = ['mse%s' % (i + 1) for i in range(len(predicted_results))]
        columns.extend(['mae%s' % (i + 1) for i in range(len(predicted_results))])
        df = pd.DataFrame([result], columns=columns)
        df = df.dropna(axis=1, how='all')
        df.to_csv(self._get_root_dir_name(ticker) + '%s_errors.csv' % ticker)

    def push_predicted_data(self, horizon, prediction_result, real_price, predicted_date):
        self.predicted_prices[horizon].append(prediction_result)
        self.real_prices[horizon].append(real_price)
        self.predicted_dates[horizon].append(str(predicted_date)[:10])

    def push_start_date(self, start_date):
        self.start_dates.append(str(start_date)[:10])

    def update_result(self):
        '''
        ticker_name: ticker name
        predicted_price : should be [[1_days_ahead_p1,1_days_ahead_p2,1_days_ahead_p3],
                                     [2_days_ahead_p1,2_days_ahead_p2,2_days_ahead_p3]]
        '''
        if self._check_list_type(self.predicted_prices) is pd.Series:
            predicteds = [[] for i in range(len(self.real_prices))]
            actuals = [[] for i in range(len(self.real_prices))]
            for i in range(len(self.real_prices)):
                for j in range(len(self.real_prices[i])):
                    actuals[i].append(self.real_prices[i][j]['Close'])

            for i in range(len(self.predicted_prices)):
                for j in range(len(self.predicted_prices[i])):
                    predicteds[i].append(self.predicted_prices[i][j]['Close'])
        else:
            predicteds = self.predicted_prices
            actuals = self.real_prices

        self.write_price_pic(self.ticker_name, predicteds, actuals, self.start_dates)
        self.write_csv(self.ticker_name, predicteds, actuals, self.start_dates, self.predicted_dates)
        self.calculate_error(self.ticker_name, predicteds, actuals, self.start_dates)

