import pandas as pd
import glob
from aicryptoSDK import AICrypto
import datetime
import numpy as np
from sklearn.metrics import mean_squared_error
import math


aicrypto = AICrypto(access_token="aWnkBW0teY")
from sklearn.metrics import mean_absolute_error
# tickers=['ACT', 'ADA']
tickers=['ACT','ADA','AION','ARK','BAT','BCD','BCH','BNB','BNT','BTC','BTCP','BTG','BTS','BTX','CND','CNX','CVC','DASH','DATA','DGB','DGD','EDG','ELF','EMC','EOS','ETC','ETH','ETHOS','ETP','FSN','FUN','GAME','GAS','GNO','GNT','HOT','ICN','ICX','IOST','KIN','KNC','LRC','LSK','LTC','MANA','MCO','MITH','MKR','NANO','NEO','NPXS','NXT','OMG','ONT','PAY','PIVX','POLY','PPT','QASH','QTUM','R','RDD','RDN','REP','REQ','RLC','SAN','SMART','SNGLS','SNT','STORJ','STRAT','SYS','TRX','USDT','VTC','WAVES','WAX','XEM','XLM','XMR','XRP','XTZ','XVG','XZC','ZEC','ZEN','ZIL','ZRX']

def get_data(ticker_name):
    df = aicrypto.read.data(
        datasource="quandl_bnc2_stock_prices+marketpsych_crypto",
        version="0.1",
        ticker=ticker_name,
        resolution="1D"
        )
    return df
sum_1 = 0
rms_list = []
for ticker in tickers:
    df = get_data(ticker)
    df_t = pd.read_csv("/home/xenon/crypto_quandl_prediction_service/crypto_quandl_prediction_service/"+ticker+"_4.csv")
    date_predict = []
    date_final = []
    date_get = df_t['timestamp_utc'].values
    date_predict.append(date_get)
    for i in range(len(df_t)):
        d1 = datetime.datetime.strptime(date_predict[0][i], "%Y-%m-%d")
        # print(d1)
        new_format = "%Y-%m-%d"
        d1.strftime(new_format)
        # print("hello",d1)
        # print(type(d1))
        l = d1.date()

        final = l
        date_final.append(d1.date())
    df_date = pd.DataFrame({'Date': date_final})
    # print(df_date)
    df_t['timestamp_utc'] = df_date
    # print(df_t)
    df_t.set_index('timestamp_utc', inplace=True)
    # print(df_t)
    # print(df.head(4))
    df_obtained = pd.merge(df, df_t, left_index=True, right_index=True)
    # print(df_obtained)
    # print(df_obtained)
    # df_results = df_obtained[['prediction_results', 'quandl_eod_stock_prices_Close']]
    # df_results.to_csv("/home/xenon/crypto_quandl_prediction_service/crypto_quandl_prediction_service/results"+ticker+".csv", index=False)
    # print(df_obtained.columns)
    y_pred = df_obtained['prediction_results']
    # print(y_pred)
    y_pred = np.array(y_pred)
    y_true = df_obtained['quandl_eod_stock_prices_Close']
    y_true = np.array(y_true)
    mae = mean_absolute_error(y_true, y_pred)
    print("mean-absolute-error", mae, ticker)
    rms = math.sqrt(mean_squared_error(y_true, y_pred))
    # print("rms", rms)
    print("rmse", rms, ticker)
    sum_1 = (sum_1)+(rms)
    # print("sum", sum_1)
    per = ((rms) / (3771.903306698503)) * (100)
    accuracy1 = ((100) - (per))
    # acc = (100) - (rms)
    print("accuracy", accuracy1, ticker)
    # print("accuracy with respect to different models", acc)
    # print("rmse in percentage", per, ticker)

#     rms_list.append(rms)
print(sum_1)
#
for ticker in tickers:
    for i in rms_list:
        per = ((i)/sum_1)*(100)
#         # print("rmse in percentage", per, ticker)







# path ='/home/xenon/crypto_quandl_prediction_service/crypto_quandl_prediction_service/' # use your path
# allFiles = glob.glob(path + "/*.csv")
# frame = pd.DataFrame()
# list_ = []
# for file_ in allFiles:
#     df = pd.read_csv(file_,index_col=None, header=0)
#     list_.append(df)
# frame = pd.concat(list_)
