"""

Watstock Models Module

"""


"""

Imports

"""
import os
os.environ['KERAS_BACKEND'] = 'theano' # set Keras to work on theano (GPU mode)
from keras.models import Sequential
from keras.layers import Input
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.recurrent import GRU
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import MaxPooling1D
from keras.layers.merge import Concatenate
from keras.models import Model

"""

Functions

"""
def create_model_cnn1(shapes, hparams, dropout):
    """
    Create Single CNN Model

    :param shapes: input layer shape
    :param hparams: list of shapes for each conv filter and dense layer
    :param dropout: dropout param for drop out layer
    :return: CNN model
    """
    #### WIll ADD STATEFUL LSTM FUNCTIONALITY BUT NEEDS BATCH_SIZE SET TO 1 WHICH GIVES ONLINE LEARNING FACILITY
    model = Sequential()
    model.add(Conv1D(filters=hparams[0], kernel_size=3, activation='relu', input_shape=(shapes[1], shapes[0])))
    model.add(MaxPooling1D(2))
    model.add(Conv1D(filters=hparams[1], kernel_size=2, activation='relu'))
    model.add(MaxPooling1D(2))
    model.add(Dropout(dropout))
    model.add(Flatten())
    model.add(Dense(hparams[2], kernel_initializer="glorot_uniform", activation='relu'))
    model.add(Dense(hparams[3], kernel_initializer="glorot_uniform", activation='sigmoid'))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
    print(model.get_weights())
    return model


def create_model_cnn2(shapes, hparams, dropout, filter_sizes=[4, 5]):
    """
    Create Multiple CNN Model

    :param shapes: input layer shape
    :param hparams: list of shapes for layers
    :param dropout: dropout param for dropout layer
    :param filter_sizes: filter sizes
    :return: CNN model
    """
    X = Input(shape=(shapes[1], shapes[0]))
    gru1 = GRU(hparams[0], return_sequences=True, name='gru1')(X)
    conv_blocks = []
    for size in filter_sizes:
        conv = Conv1D(filters=hparams[0],
                      kernel_size=size,
                      padding="valid",
                      activation="relu",
                      strides=1)(gru1)
        conv = MaxPooling1D(pool_size=2)(conv)
        conv = Flatten()(conv)
        conv_blocks.append(conv)
    z = Concatenate()(conv_blocks) if len(conv_blocks) > 1 else conv_blocks[0]

    z = Dropout(dropout)(z)
    z = Dense(hparams[1], activation="relu")(z)
    model_output = Dense(1, activation="linear")(z)

    model = Model(X, model_output)
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model


def create_model_lstm(shapes, hparams, dropout):
    """
    Create Single LSTM Model

    :param shapes: shape for input layer
    :param hparams: list of shape for layers
    :param dropout: dropout param for dropout layer
    :return: LSTM model
    """
    #### WIll ADD STATEFUL LSTM FUNCTIONALITY BUT NEEDS BATCH_SIZE SET TO 1 WHICH GIVES ONLINE LEARNING FACILITY
    model = Sequential()
    model.add(GRU(hparams[0], input_shape=(shapes[1], shapes[0]), return_sequences=True))
    model.add(Dropout(dropout))

    model.add(GRU(hparams[1], input_shape=(shapes[1], shapes[0]), return_sequences=False))
    model.add(Dropout(dropout))

    model.add(Dense(hparams[2], kernel_initializer="glorot_uniform", activation='relu'))
    model.add(Dense(hparams[3], kernel_initializer="glorot_uniform", activation='linear'))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model

