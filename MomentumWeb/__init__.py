"""
The flask application package.
"""
from flask import Flask
from MomentumWeb.src.constants import *
from MomentumWeb.src import utils
app = Flask(__name__)

import MomentumWeb.views
