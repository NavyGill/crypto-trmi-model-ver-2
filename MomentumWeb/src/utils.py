from datetime import date, timedelta
from aicrypto_telegramsdk import AITelegram
import re
from time import sleep
from nltk.sentiment.vader import SentimentIntensityAnalyzer

from MomentumWeb.src.filters import *


def check_date(time, path, is_first):
    if is_first:
        yest = time - timedelta(days=1)
        predict_sent(yest.year, yest.month, yest.day, yest.year, yest.month, yest.day, flag=1, path=path)
        print(yest)
    if date.today() == time:
        sleep(3600)
    else:
        predict_sent(time.year, time.month, time.day, time.year, time.month, time.day, flag=1, path=path)
        print(time)
        time = date.today()
    return time


def predict_sent(year, month, day, end_y, end_m, end_d, flag=0, path=None):
    aitelegram = AITelegram(access_token="aWnkBW0teY")

    year, month, day, end_y, end_m, end_d = int(year), int(month), int(day), int(end_y), int(end_m), int(end_d)

    df = aitelegram.read.data(datasource="telegram_datasets", version="1.0", startdate=date(year, month, day),
                              enddate=date(end_y, end_m, end_d))

    regex = r"https"
    subset = ""

    df.dropna(inplace=True)
    print("hello",df)

    removed_links = list(map(lambda x: re.sub(regex, subset, x), list(df['message'])))
    removed_links = list(map(str.strip, removed_links))
    print(removed_links, "removed-links")
    df['message'] = removed_links
    print("links-removed", df)

    remove_punctuation = string.punctuation
    print(remove_punctuation, "punnnnnnnnnnnnnnnnnnnnn")
    print(len(remove_punctuation), "ghjkskjaknjhxzbhb")

    tweet_list = list(df['message'])

    set_list = []
    clean_tweet_list = []
    translator = str.maketrans(remove_punctuation, ' ' * len(remove_punctuation))  # punctuation remover
    print(translator, "trans-----------")
    for word in tweet_list:
        list_form = word.split()  # turns the word into a list

        to_process = [x for x in list_form if not x.startswith("@")]  # removes handles
        print(to_process,"toooooooooooooooooooo")

        to_process_2 = [x for x in to_process if not x.startswith("RT")]  # removed retweet indicator
        print(to_process_2, "to22222222222222222222222222222222")

        string_form = " ".join(to_process_2)  # back into a string
        print(string_form, "yyyyyyyyyyyyyyyyyyyyyy")

        set_form = set(
            string_form.translate(translator).strip().lower().split())  # creating a set of words in the tweet
        print(set_form, "ghjkoeeeeeeeeeeeeeeeeeeeeeeeeee")

        clean_tweet_list.append(string_form.translate(translator).strip().lower())

        set_list.append(tuple(set_form))  # need to make it a tuple so it's hashable

    df['tuple_version_tweet'] = set_list
    df['clean_tweet_V1'] = clean_tweet_list
    # print('punnnvvvvvvvvvvvvvvvv', df)

    df = filtration_1(df, "clean_tweet_V1", "clean_tweet_V2")
    print(df, "cleaned_df")
    df = filtration_2(df, "clean_tweet_V2")

    df = df.drop(['tuple_version_tweet', 'clean_tweet_V1', 'message'], axis=1)

    sentiment = SentimentIntensityAnalyzer()

    tweets = df

    positive_sentiment = []
    negative_sentiment = []
    neutral_sentiment = []
    compound_sentiment = []

    for i in range(tweets.shape[0]):
        sent_dict = sentiment.polarity_scores(tweets.iloc[i, 1])

        positive_sentiment.append(sent_dict['pos'])
        negative_sentiment.append(sent_dict['neg'])
        neutral_sentiment.append(sent_dict['neu'])
        compound_sentiment.append(sent_dict['compound'])

    tweets['pos_sent'] = positive_sentiment
    tweets['neg_sent'] = negative_sentiment
    tweets['neu_sent'] = neutral_sentiment
    tweets['compound_sent'] = compound_sentiment

    tweets['date'] = tweets.index.date
    features = ["compound_sent"]

    modelling_df = tweets.groupby(["date"]).mean()[features]
    modelling_df.reset_index(inplace=True)
    if flag == 1:
        data = pd.read_csv(path + "/MomentumWeb/data/data.csv")
        data = pd.concat([data, modelling_df], ignore_index=True, join='inner')
        data.drop_duplicates(subset='date', inplace=True, keep=False)
        data.to_csv(path + "/MomentumWeb/data/data.csv", index=False)
    print(modelling_df)
    return modelling_df['compound_sent']
