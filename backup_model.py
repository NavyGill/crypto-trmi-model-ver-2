import boto3
import datetime
from boto.s3.connection import S3Connection
import os
from glob import glob

# to use this function, you need to install aws-cli first and configure the credentials.
# for more detail, please refer to orange(huang.z@watstock.com)


class ModelBackupUtil:
    PREDICTION_S3_BUCKET_NAME = "ticker-model-backup"
    # MODEL_PATH = "models"
    MODEL_PATH = "/home/watstock/modules_combined/%s/backtest_model/"

    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.bucket = self.s3.Bucket(self.PREDICTION_S3_BUCKET_NAME)

    def _get_s3_uri(self, bucket, key):
        return 'https://' + bucket + '.s3.amazonaws.com/' + key;

    def upload_file(self, file_name):
        # build key
        print ('uploading file %s' % file_name)
        now = datetime.datetime.utcnow()
        date = now.strftime('%Y-%m-%d')
        key = '%s/%s' % (date, file_name)

        try:
            self.s3.meta.client.upload_file(file_name, self.PREDICTION_S3_BUCKET_NAME, key)
            print ('successfully uploaded file %s' % file_name)
        except:
            print ("error uploading file %s" %file_name)

        return self._get_s3_uri(self.PREDICTION_S3_BUCKET_NAME, key)

    def backup_model(self):
        file_lists = [y for x in os.walk(self.MODEL_PATH) for y in glob(os.path.join(x[0], '*.*'))]
        print 'totally %d files will be uploaded into s3 bucket' % len(file_lists)
        index = 0
        for f in file_lists:
            index += 1
            print '\n-----------current progress %d/%d------------'%(index,len(file_lists))
            self.upload_file(f)


#todo:not tested
class ModelRecovery():
    PREDICTION_S3_BUCKET_NAME = "ticker-model-backup"
    MODEL_PATH = "/home/watstock/modules_combined/%s/backtest_model/"

    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.bucket = self.s3.Bucket(self.PREDICTION_S3_BUCKET_NAME)

    def _get_s3_uri(self, bucket, key):
        return 'https://' + bucket + '.s3.amazonaws.com/' + key;

    def download_file(self, file_name):
        self.s3.meta.client.download_file(self.PREDICTION_S3_BUCKET_NAME, 'hello.txt', '/tmp/hello.txt')


#todo: not finished
class BucketFileRetriever():
    MODEL_PATH = "/home/watstock/modules_combined/%s/backtest_model/"

    def __init__(self,bucket_name):
        self.s3 = boto3.resource('s3')
        #todo: not setting the access key and secret-acess-key
        conn = S3Connection('access-key', 'secret-access-key')
        self.bucket = conn.get_bucket('bucket')
        # self.bucket = self.s3.Bucket(bucket_name)

    def get_all_file(self):
        for key in self.bucket.list():
            print key.name.encode('utf-8')


