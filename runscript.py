from MomentumWeb.src.utils import check_date
from datetime import date
import os


path = os.getcwd()
time = date.today()
is_first = True
while True:
    time = check_date(time, path, is_first)
    is_first = False
